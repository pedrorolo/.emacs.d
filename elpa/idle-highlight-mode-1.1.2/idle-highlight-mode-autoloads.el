;;; idle-highlight-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (idle-highlight-mode) "idle-highlight-mode" "idle-highlight-mode.el"
;;;;;;  (20781 62460 113693 145000))
;;; Generated autoloads from idle-highlight-mode.el

(autoload 'idle-highlight-mode "idle-highlight-mode" "\
Idle-Highlight Minor Mode

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("idle-highlight-mode-pkg.el") (20781 62460
;;;;;;  178862 112000))

;;;***

(provide 'idle-highlight-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; idle-highlight-mode-autoloads.el ends here
